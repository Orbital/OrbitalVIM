-- this is the start point, neovim when started will read this file and load everything specified in here

-- load required modules
require "orbitalVIM.settings" --load general settings
require "orbitalVIM.keys"     --load keymappings
require "orbitalVIM.plugins"  --load plugins
require "orbitalVIM.colors"   --load colorschemes
require "config.cmp"   --load nvim-cmp configuration
require "orbitalVIM.lsp" --load all lsp stuff
require "orbitalVIM.treesitter" --load treesitter
require "orbitalVIM.nvimtree" --load nvimtree
require "orbitalVIM.bufferline" --load bufferline

