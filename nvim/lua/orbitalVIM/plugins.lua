-- This file contains the configuration for packer.nvim & list of plugins to be installed

-- shorten function name
local fn = vim.fn

-- Automatically install neovim plugin manager packer.nvim on first startup
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call to load packer so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  vim.notify("Could not load Packer.nvim")
  return
end

-- Initialise Packer
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "single" } 
    end,
  },
}



-- Install your plugins here
return packer.startup(function(use)
  -- OrbitalVIM plugins
  use "wbthomason/packer.nvim" -- Have packer update & manage itself
  use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim used by lots of plugins
  use "nvim-lua/plenary.nvim" -- Useful lua functions used by lots of plugins
  use "joshdick/onedark.vim" -- color scheme

  -- cmp plugins
  use "hrsh7th/nvim-cmp" -- The completion plugin
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "saadparwaiz1/cmp_luasnip" -- snippet completions

  -- Treesitter 
  use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  }
  use "p00f/nvim-ts-rainbow"
  
  -- snippets
  use "L3MON4D3/LuaSnip" --snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  -- LSP 
  use "neovim/nvim-lspconfig" -- enable LSP
  use "williamboman/nvim-lsp-installer" -- simple to use language server installer
  use "hrsh7th/cmp-nvim-lsp" -- enable nvim-cmp autocompletions for LSP
  
  -- Nvim Tree
  use {
    "kyazdani42/nvim-tree.lua",
    requires = { "kyazdani42/nvim-web-devicons", opt = true }
  }

  -- Bufferline  
  use {
    "akinsho/bufferline.nvim",
    requires = { {"kyazdani42/nvim-web-devicons", opt = true}, {"moll/vim-bbye" } }
  }

  -- Custom plugins go here
  

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
