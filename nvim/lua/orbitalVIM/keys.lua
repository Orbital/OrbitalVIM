-- this file contains default keybindings set by orbital VIM 

-- Index
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",
--   carrage return (pressing enter) = cr
--   Control = C
--   Shift = S
--   Alt = A
--
-- Syntax for setting keymaps : keymap_function_name("mode",<custom-binding>,<original-binding/command><cr>, opts)

-- Neovim settings for setting keymaps 
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

-- Shorten keymap function name to 'keymap' for setting keymaps
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "


-- BASIC OPERATIONS --

-- Copy whole file content
-- use Ctrl+ a to copy the whole file
keymap("n", "<C-a>", ":%y+ <CR>", opts) 

-- Create a new buffer
-- use Shift + t to create a new buffer
keymap("n", "<S-t>", ":enew <CR>", opts) 

-- Create a a new tab
-- use Ctrl + t to create a new tab
keymap("n", "<C-t>", ":tabnew <CR>", opts) 

-- Save file
-- use Ctrl + s to save current file
keymap("n", "<C-s>", ":w <CR>", opts) 

-- Save all files and close current buffer
-- use Ctrl + x to save and close files in current buffer
keymap("n", "<C-x>", ":bw! <CR>", opts) 

-- Save all files and quit neovim
-- use Ctrl + q to save files across all open buffers and quit neovim
keymap("n", "<C-q>", ":wqa <CR>", opts) 

-- Force quit
-- use Ctrl + Alt + q to close all buffers and quit neovim without saving any edited files
keymap("n", "<C-A-q>", ":qa! <CR>", opts) 


-- NORMAL MODE --

-- Better window navigation
-- use Ctrl + h/j/k/l to move across split windows within neovim
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Resize with arrow keys
-- use Ctrl + Up/Right to increase split window sizes
-- use Ctrl + Down/Left to increase split window sizes
keymap("n", "<C-Up>", ":resize +2<CR>", opts)             --horizontal windows
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts) --vertical windows
keymap("n", "<C-Down>", ":resize -2<CR>", opts)           --horizontal windows
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)  --vertical windows

-- Navigate buffers
-- use Shift+L/R to move between next and previous buffers respectively
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)


-- INSERT MODE --

-- Press jk fast to switch from insert mode to normal mode
keymap("i", "jk", "<ESC>", opts)


-- VISUAL MODE --

-- Stay in indent mode
-- Keep tabbing on selected text in visual mode on pressing >/<
keymap("v", ">", ">gv", opts) --increase tabs
keymap("v", "<", "<gv", opts) --decrease tabs

-- Move text up and down
-- Use Alt + J/K to move selected text up & down respectively
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)


-- VISUAL BLOCK MODE --

-- Move text up and down
-- Use Alt + J/K to move selected text (in visual block) up & down respectively
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)


-- TERMINAL --

-- Better terminal navigation
-- use Ctrl + h/j/k/l to move across open terminals within neovim
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)


-- MISC --

-- Disable neovim from copying text over which text a copied string was pasted, instead hold on to the originally copied string in clipboard
keymap("v", "p", '"_dP', opts)

-- Toggle numbers
-- use Leader + n to toggle numbers
keymap("n", "<leader>n", ":set nu! <CR>", opts)


-- PLUGIN SPECIFIC --

-- File explorer
-- use leader + e to open file explorer
--keymap("n", "<leader>e", ":Lex 30<cr>", opts)
keymap("n", "<leader>e", ":NvimTreeToggle<cr>", opts)
