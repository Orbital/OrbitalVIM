-- this file tells neovim which colorscheme is to be used

local colorscheme = "onedark"

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme " .. colorscheme .. " was not found! Resorting to default colorscheme")
  return
end
