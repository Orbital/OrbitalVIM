
<p align="center" >
<img src="./.codeberg/orbitalVIM.png" width="60" />
</p>

<h1 align="center">Orbital VIM</h1>

<div align="center">

![neovim-version](https://img.shields.io/static/v1?label=Neovim&message=0.7.0&style=for-the-badge&logo=Neovim&color=9cf)
![platform](https://img.shields.io/static/v1?label=Platform&message=Linux&style=for-the-badge&logo=linux&color=9cf)
![status](https://img.shields.io/static/v1?label=Status&message=Work-in-progress&style=for-the-badge&logo=Codeberg&color=9cf)
![release](https://img.shields.io/static/v1?label=Release&message=Alpha&style=for-the-badge&logo=Git&color=9cf)

</div>

<p align="center">Custom Neovim configuration focusing on productivity & simplicity, supporting completions, fuzzy finders, status lines and more out of the box. </p>

<br/>

## Features 

* 100% Lua configuration
* Supports user's custom keymaps,neovim settings & plugin configurations
* Uses Packer.nvim as the default package manager
* Supports lazy loading plugins
* Multiple colorschemes out of the box
* Supports auto completion with neovim's built in LSP and nvim-cmp
* Uses Treesitter for syntax highlighting
* Uses Null-LS for syntax formating & linting
* Uses Teliscope for fuzzy-finding
* Uses NvimTree as default file explorer 

And a lot more :)

<div align="center">
<h2 align="center">Get started</h2>

<table>
<tr><th>Installation</th><th>Documentation</th></tr>
<tr><td>

| Follow the steps below to install Orbital VIM | 
|-----------------------------------------------|
| [Setup Dependencies]                          |
| Install OrbitalVIM                            |
| Configure a basic setup for use               |

</td><td>

| Learn more and customize Orbital VIM for yourself | 
|---------------------------------------------------------|
| [Keybindings]                                           |
| [File structure]                                        |
| Plugin Configuration                                    |

</td></tr> </table>
</div>

## Dependencies

To run orbitalVIM along with [neovim 0.7.0+] you will also need the following dependencies installed in your system :

<div align="center">

| Package Name      | Description                        | Installation                     | Requirement |
|-------------------|:-----------------------------------|:---------------------------------|:-----------|
| [libstdc++]       | C++ library required by Treesitter to compile parsers   | System package-manager dependent | **Essential**  |
| [libstdc++-devel] | Development files for libstdc++    | System package-manager dependent | **Essential**   |
| [libstdc++-static]| Static files for libstdc++         | System package-manager dependent | **Essential**, ONLY REQUIRED by `Fedora`/`Redhat` based distributions along with `libstdc++` & `libstdc++-devel`   |
| [gcc]             | GNU Compiler required by Treesitter to compile parsers | System package-manager dependent | **Essential** |
| [gcc-c++]         | C++ support for gcc        | System package-manager dependent | **Essential**, ONLY REQUIRED by `Fedora`/`Redhat` based distributions along with `gcc`  |
| [xsel]            | Adds clipboard support to Neovim   | System package-manager dependent | **Essential**   |
| [pynvim]          | Adds Python support to Neovim      | `pip install pynvim`             | Optional    |
| [neovim]          | Adds nodejs support to Neovim      | `sudo npm i -g neovim`           | Optional    |

</div>

Run the command below to check wether the dependencies have been installed properly
```
nvim -c "checkhealth"
```

## Installation

This is **Experimental**. Unless you are developing Orbital VIM **DO NOT** install. Please wait for v1.0.0 release. 
```bash
cd ~/.config && mkdir nvim && cd ~/.config/nvim && git clone https://codeberg.org/Orbital/OrbitalVIM.git &&  mv OrbitalVIM/* ./ && nvim -c "PackerSync"
```

[neovim 0.7.0+]: https://pkgs.org/search/?q=neovim
[libstdc++]: https://pkgs.org/search/?q=libstdc%2B%2B
[libstdc++-devel]: https://pkgs.org/search/?q=libstdc%2B%2B-devel
[libstdc++-static]: https://pkgs.org/search/?q=libstdc%2B%2B-static
[gcc]: https://pkgs.org/search/?q=gcc
[gcc-c++]: https://pkgs.org/search/?q=gcc-c%2B%2B
[xsel]: https://pkgs.org/search/?q=xsel
[pynvim]: https://pypi.org/project/pynvim/
[neovim]: https://www.npmjs.com/package/neovim

[Setup Dependencies]: #dependencies
[Keybindings]: https://codeberg.org/Orbital/OrbitalVIM/wiki/Keybindings
[File structure]: https://codeberg.org/Orbital/OrbitalVIM/wiki/File-Structure
