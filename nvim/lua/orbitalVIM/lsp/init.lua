-- check if lsp is available
local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  vim.notify("lsp-config was not found")
	return
end

require("orbitalVIM.lsp.lsp-installer")
require("orbitalVIM.lsp.handlers").setup()


